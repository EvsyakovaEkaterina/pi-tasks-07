#pragma once
#ifndef PRM
#define PRM
#include "Employee.h"
#include "Heading.h"
#include "Manager.h"
#include <iostream>
#include <string>
using namespace std;

class ProjectManager : public Manager, public Heading
{
protected:
	int number;
	int stakeFor1;
public:
	ProjectManager() {};
	ProjectManager(string id, string name, int workTime, double payment, string projectName, double part, int budget,
		int number, int stakeFor1) : Manager(id, name, workTime, payment, projectName, part, budget)
	{
		this->number = number;
		this->stakeFor1 = stakeFor1;
	}

	int EmplNumbSalary(int stakeFor1, int number) override
	{
		return stakeFor1 * number;
	}

	void CalcPayment() override
	{
		SetPayment(EmplNumbSalary(stakeFor1, number) + ProjectSalary(budget, part));
	}

	void Print_Info() override
	{
		cout << endl << "Id: " << GetId() << endl << "Name: " << GetName() << endl << "Profession: Project Manager" << endl
			<< "Project Name: " << projectName << endl << "Budget: " << budget << endl << "Part: " << part << endl
			<< "Employees Number: " << number << endl << "Stake for one: " << stakeFor1 << endl
			<< "Work Time: " << GetWorkTime() << " hours" << endl << "Salary: " << GetPayment() << endl;
	}
};
#endif