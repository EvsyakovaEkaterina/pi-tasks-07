#pragma once
#define per 100
#define amount 1000
#define ratio1 0.95
#define ratio2 0.9
#define ratio3 0.8
class WorkTime
{
	public:
		WorkTime() {}
		~WorkTime() {}
		virtual double salary_type_1(int worktime, int base) = 0; 
		// { return worktime*base; }
};

class Project
{
	public:
		Project() {}
		~Project() {}
		virtual double salary_type_2(int budget, double part) = 0;
		// { return budget*part / per; }
};

class Heading
{
	public:
		Heading() {}
		~Heading() {}
		virtual double salary_type_3(int qual) = 0;
		// { return qual*amount; }
};