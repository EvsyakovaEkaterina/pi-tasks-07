#pragma once
#include "Interface.h"
#include "Employee.h"

class Personal : public Employee, public Worktime 
{
protected:
	int base;
public:
	Personal() {};

	Personal(int id, string name, int base)
	{
		this->id = id;
		this->name = name;
		this->base = base;
	}

	int PayTime(int time, int base) { return time * base; }
};

class Cleaner : public Personal
{
public:
	Cleaner(int id, string name, int base)
	{
		this->id = id;
		this->name = name;
		this->base = base;
		payment = 0;
	}

	void Pay()
	{
		payment = PayTime(time, base );
	}
	
};

class Driver : public Personal
{
public:
	Driver(int id, string name, int base)
	{
		this->id = id;
		this->name = name;
		payment = 0;
		this->base = base;
	}

	void Pay()
	{
		payment = PayTime(time, base);
	}
};