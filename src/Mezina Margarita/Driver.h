#pragma once
#include "Personal.h"
class Driver : public Personal
{
public:
	Driver(int id = 0, string fio = "", int base = 0, int worktime = 0, int payment = 0) {
		this->id = id;
		this->fio = fio;
		this->base = base;
		this->payment = payment;
		this->worktime = worktime;
	}

	void SetPayment() {
		this->payment = EstimationWorkTime(this->worktime, this->base)*0.95;
	}

	~Driver() {
	}
};

